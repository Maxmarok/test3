<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<title>Тестовая страница</title>
<h1>Тестовая страница</h1>
<div>
    <h3>Сегодня: <?=date("d.m.Y")?></h3>
    <h4>Мероприятия сегодня в Екатеринбурге</h4>
    <?=getEvents(true, 'Екатеринбург');?>
    <br />
    <br />
    <h4>Мероприятия сегодня</h4>
    <?=getEvents(true);?>
    <br />
    <br />
    <h4>Все мероприятия</h4>
    <?=getEvents();?>
    <br />
</div>


<?

function getEvents($currentDate = false, $city = false)
{
    $content = '';

    $arFilter = Array(
        "ACTIVE" => "Y",
        "IBLOCK_CODE" => 'events',
        "IBLOCK_TYPE" => 'events',
    );

    /* Условие для текущей даты */
    if ($currentDate) {
        $arFilter[">=" . "PROPERTY_DATE_BEGIN"] = date("Y-m-d");
    }

    /* Условие для конкретного города */
    if ($city) {
        $arFilter["PROPERTY_LOCATION.NAME"] = $city;
    }

    /* Берем только необходимое */
    $arSelect = Array(
        "ID",
        "NAME",
        "PROPERTY_DATE_BEGIN",
        "PROPERTY_DATE_END",
        "PROPERTY_LOCATION",
        "PROPERTY_MEMBERS"
    );

    if (CModule::IncludeModule("iblock")) {
        $res = CIBlockElement::GetList(
            Array("SORT" => "DESC"),
            $arFilter,
            false,
            false,
            $arSelect
        );


        while ($events = $res->GetNextElement()) {
            $event = $events->GetFields();

            /* Узнаем привязанный город */
            if (!empty($event["PROPERTY_LOCATION_VALUE"])) {
                $city = CIBlockElement::GetList(
                    Array("SORT" => "ASC"),
                    Array(
                        "ACTIVE" => "Y",
                        "IBLOCK_CODE" => 'cities',
                        "IBLOCK_TYPE" => 'cities',
                        "ID" => $event["PROPERTY_LOCATION_VALUE"]
                    ),
                    false,
                    false,
                    Array("NAME")
                )->Fetch();

                $location = $city["NAME"];
            } else {
                $location = 'Не назначено';
            }


            /* Узнаем участников */
            if (!empty($event["PROPERTY_MEMBERS_VALUE"])) {

                $party = '';
                $mem = CIBlockElement::GetList(
                    Array("SORT" => "ASC"),
                    Array(
                        "ACTIVE" => "Y",
                        "IBLOCK_CODE" => 'members',
                        "IBLOCK_TYPE" => 'members',
                        "ID" => $event["PROPERTY_MEMBERS_VALUE"]
                    ),
                    false,
                    false,
                    Array("NAME")
                );

                while ($members = $mem->GetNextElement()) {
                    $member = $members->GetFields();
                    $party[] = $member["NAME"];
                }

                $party = implode(', ', $party);

            } else {
                $party = 'Участников не заявлено';
            }

            /* Выводим контент */
            $content .= '<div style="margin: 10px 0;">
                    <div>Мероприятие: ' . $event["NAME"] . '</div>
                    <div>Дата начала: ' . $event["PROPERTY_DATE_BEGIN_VALUE"] . '</div>
                    <div>Дата окончания: ' . $event["PROPERTY_DATE_END_VALUE"] . '</div>
                    <div>Место проведения: ' . $location . '</div>
                    <div>Участники: ' . $party . '</div>
                    </div>';
        }
    }
    return $content;
}
?>